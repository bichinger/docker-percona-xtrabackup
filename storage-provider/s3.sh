#!/usr/bin/env bash
set -e

LOCAL_FILE=$1
S3_FILE=$2

EXIT_CODE=0
# check if env var exists
check_env_var() {
    env_var=$1
    env_var_name=$2
    if [ -z "$env_var" ]; then
      echo -e "[failed]"
      echo -e "--> ${env_var_name} not defined."
      echo -e ""
      EXIT_CODE=1
    fi
}


echo -ne "* Checking S3 ENV variables...\t"
# check env vars
check_env_var "$S3_ACCESS_KEY_ID" "S3_ACCESS_KEY_ID"
check_env_var "$S3_SECRET_ACCESS_KEY" "S3_SECRET_ACCESS_KEY"
check_env_var "$S3_BUCKET" "S3_BUCKET"
check_env_var "$S3_REGION" "S3_REGION"

if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi
if [ "$EXIT_CODE" == 0 ]; then echo -e "[OK]"; fi


# env vars needed for aws tools
export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

push_file_to_s3 () {
  SRC_FILE=$1
  DEST_FILE=$2

  echo -ne "* Uploading ${DEST_FILE} to S3......\t"
  aws s3  cp $SRC_FILE s3://$S3_BUCKET/$DEST_FILE

  if [ $? != 0 ]; then
    echo -e "[failed]"
    >&2 echo -e "--> Upload failed..."
    echo -e ""
    exit 1
  fi
  rm $SRC_FILE
  echo -e "[OK]"
}

push_file_to_s3 $LOCAL_FILE $S3_FILE
FROM phusion/baseimage:0.9.22

RUN apt-get update -qq \
    && apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
    && apt-get install -y --no-install-recommends wget python-setuptools \
    && wget https://repo.percona.com/apt/percona-release_0.1-4.$(lsb_release -sc)_all.deb \
    && dpkg -i percona-release_0.1-4.$(lsb_release -sc)_all.deb \
    && apt-get update -qq \
    && apt-get install -y --no-install-recommends percona-xtrabackup-24 mysql-client libdbd-mysql-perl rsync libaio1 libcurl3 libev4 bzip2 xz-utils gzip python-pip sshpass \
    && pip install --upgrade setuptools wheel \
    && pip install awscli \
    && mkdir -p /etc/my_init.d/ \
    && mkdir -p /root/xtrabackup/storage-provider \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY storage-provider/*.sh /root/xtrabackup/storage-provider/
COPY backup.sh /root/xtrabackup/backup.sh

WORKDIR /root/xtrabackup

RUN touch /etc/service/syslog-forwarder/down

CMD ["/sbin/my_init", "--", "/root/xtrabackup/backup.sh"]
